#!/bin/bash


gcc -Wall pipeline.c /usr/local/lib/libnanomsg.a -lc -lpthread -lanl -o pipeline

gcc -Wall reqrep.c /usr/local/lib/libnanomsg.a -lc -lpthread -lanl -o reqrep

gcc -Wall pair.c /usr/local/lib/libnanomsg.a -lc -lpthread -lanl -o pair

gcc -Wall pubsub.c /usr/local/lib/libnanomsg.a -lc -lpthread -lanl -o pubsub

gcc -Wall survey.c /usr/local/lib/libnanomsg.a -lc -lpthread -lanl -o survey

gcc -Wall bus.c /usr/local/lib/libnanomsg.a -lc -lpthread -lanl -o bus
