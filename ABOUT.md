NanoMSG examples
================


 * nanomsg -  socket library that provides several common communication patterns. (http://nanomsg.org/)[http://nanomsg.org/]

     Documentation: http://nanomsg.org/v0.2/nanomsg.7.html
     
  examples from:
	http://tim.dysinger.net/posts/2013-09-16-getting-started-with-nanomsg.html

  
Protocols
---------

 * PAIR - simple one-to-one communication
 * BUS - simple many-to-many communication
 * REQREP - allows to build clusters of stateless services to process user requests
 * PUBSUB - distributes messages to large sets of interested subscribers
 * PIPELINE - aggregates messages from multiple sources and load balances them among many destinations
 * SURVEY - allows to query state of multiple applications in a single go

Transport
---------

Scalability protocols are layered on top of the transport layer in the network stack. At the moment, the nanomsg library supports the following transports mechanisms:

 * INPROC - transport within a process (between threads, modules etc.)
 * IPC - transport between processes on a single machine
 * TCP - network transport via TCP

The library exposes a BSD-socket-like C AP

NOTICE
------
  Firstly, You need to download, compile/build and install library on your linux/unix machine !
